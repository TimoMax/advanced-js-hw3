// Деструктуризація - це процес розбиття складної структури даних, такої як масив або об'єкт, на окремі елементи. 
// Вона дозволяє зручно отримувати доступ до цих елементів та присвоювати їх змінним.
// Це спрощує роботу з даними, поліпшує читабельність коду та сприяє швидкому написанню програм.


// 1----------------------------------
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
const concat = [...new Set([...clients1, ...clients2])];
console.log("task#1:");
console.log(concat);

// 2-----------------------------------

const characters = [
  {
    name: "Елена",
    lastName: "Гилберт",
    age: 17,
    gender: "woman",
    status: "human"
  },
  {
    name: "Кэролайн",
    lastName: "Форбс",
    age: 17,
    gender: "woman",
    status: "human"
  },
  {
    name: "Аларик",
    lastName: "Зальцман",
    age: 31,
    gender: "man",
    status: "human"
  },
  {
    name: "Дэймон",
    lastName: "Сальваторе",
    age: 156,
    gender: "man",
    status: "vampire"
  },
  {
    name: "Ребекка",
    lastName: "Майклсон",
    age: 1089,
    gender: "woman",
    status: "vempire"
  },
  {
    name: "Клаус",
    lastName: "Майклсон",
    age: 1093,
    gender: "man",
    status: "vampire"
  }
];
const charactersShortInfo = characters.map(({ name, lastName, age }) => ({ name, lastName, age }))
console.log("task#2:");
console.log(charactersShortInfo);

// 3----------------------------------
const user1 = {
  name: "John",
  years: 30
};
const { name, years, isAdmin = "false" } = user1;

console.log("task#3:");
console.log(`name: ${name}`, `years: ${years}`, `isAdmin: ${isAdmin}`);

// 4----------------------------------
const satoshi2020 = {
  name: 'Nick',
  surname: 'Sabo',
  age: 51,
  country: 'Japan',
  birth: '1979-08-21',
  location: {
    lat: 38.869422,
    lng: 139.876632
  }
}

const satoshi2019 = {
  name: 'Dorian',
  surname: 'Nakamoto',
  age: 44,
  hidden: true,
  country: 'USA',
  wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
  browser: 'Chrome'
}

const satoshi2018 = {
  name: 'Satoshi',
  surname: 'Nakamoto',
  technology: 'Bitcoin',
  country: 'Japan',
  browser: 'Tor',
  birth: '1975-04-05'
};
const fullProfile = {
  ...satoshi2018,
  ...satoshi2019,
  ...satoshi2020,
};
console.log("task#4:");
console.log(fullProfile);

// 5---------------------------------------
const books = [{
  name: 'Harry Potter',
  author: 'J.K. Rowling'
}, {
  name: 'Lord of the rings',
  author: 'J.R.R. Tolkien'
}, {
  name: 'The witcher',
  author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
  name: 'Game of thrones',
  author: 'George R. R. Martin'
};
const newBooks = [...books, bookToAdd];
// const newBooks = books.concat(bookToAdd);
console.log("task#5:");
console.log(newBooks);

// 6-------------------------------------
const employee = {
  name: 'Vitalii',
  surname: 'Klichko'
};
const newEmployee = { ...employee, age: 30, salary: 1000 };
console.log("task#6:");
console.log(newEmployee);

// 7---------------------------------------
const array = ['value', () => 'showValue'];


const [value, showValue] = array

console.log("task#7:");
console.log(value); // має бути виведено 'value'
console.log(showValue());  // має бути виведено 'showValue'